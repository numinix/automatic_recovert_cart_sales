SET @configuration_group_id=0;
SELECT @configuration_group_id:=configuration_group_id
FROM configuration
WHERE configuration_key LIKE 'AUTOMATIC_RECOVER_CART_SALES%'
LIMIT 1;
DELETE FROM configuration WHERE configuration_group_id = @configuration_group_id AND configuration_group_id <> 0;
DELETE FROM configuration_group WHERE configuration_group_id = @configuration_group_id AND configuration_group_id <> 0;

# admin pages
DELETE FROM admin_pages WHERE page_key = 'configAUTOMATIC_RECOVER_CART_SALES';
DELETE FROM coupons WHERE coupon_code = 'coupkb';
DELETE FROM coupons_description WHERE coupon_name = 'template_recovercart';

# for complete uninstall
ALTER TABLE customers_basket DROP COLUMN first_reminder;
ALTER TABLE customers_basket DROP COLUMN second_reminder;

SET @configuration_group_id=0;
SELECT @configuration_group_id:=configuration_group_id
FROM configuration
WHERE configuration_key LIKE 'RCS\_%'
LIMIT 1;
DELETE FROM configuration WHERE configuration_group_id = @configuration_group_id AND configuration_group_id <> 0;
DELETE FROM configuration_group WHERE configuration_group_id = @configuration_group_id AND configuration_group_id <> 0;

# admin pages
DELETE FROM admin_pages WHERE page_key = 'config_recover_cart_sales';
DELETE FROM admin_pages WHERE page_key = 'recover_cart_sales';
DELETE FROM admin_pages WHERE page_key = 'stats_recover_cart_sales';

# for complete uninstall
DROP TABLE IF EXISTS scart;
DROP TABLE IF EXISTS second_scart;
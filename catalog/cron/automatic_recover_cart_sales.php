<?php
require('../includes/configure.php');
ini_set('include_path', DIR_FS_CATALOG . PATH_SEPARATOR . ini_get('include_path'));
chdir(DIR_FS_CATALOG);

// $cid value checks if coming from admin/recover_cart_sales.php
if (!isset($cid)){
	require('includes/application_top.php');
}

$emails_sent = 0;

@define('TABLE_SCART', DB_PREFIX . 'scart');
@define('TABLE_SECOND_SCART', DB_PREFIX . 'second_scart');
// sql:
# ALTER TABLE 'customers_basket' ADD 'first_reminder' TINYINT( 1 ) NOT NULL DEFAULT '0';

// clear reminders after 30 days
//clearReminders();

// why do we need to run this function?  Running it causes spam emails
function clearReminders() {
	global $db;
	$db->Execute("UPDATE " . TABLE_CUSTOMERS_BASKET . " SET first_reminder = 0 WHERE customers_basket_date_added <= CURDATE() - INTERVAL 30 DAY;");
}

function zen_create_coupon_code_rcs($salt="secret", $length = SECURITY_CODE_LENGTH) {
	global $db;
	$ccid = md5(uniqid("", $salt));
	$ccid .= md5(uniqid("", $salt));
	$ccid .= md5(uniqid("", $salt));
	$ccid .= md5(uniqid("", $salt));
	srand((double)microtime()*1000000); // seed the random number generator
	$random_start = @rand(0, (128-$length));
	$good_result = 0;
	while ($good_result == 0) {
		$id1=substr($ccid, $random_start,$length);
		$query = "select coupon_code
                from " . TABLE_COUPONS . "
                where coupon_code = '" . $id1 . "'";

		$rs = $db->Execute($query);

		if ($rs->RecordCount() == 0) $good_result = 1;
	}
	return $id1;
}

function zen_get_info_page_rcs($zf_product_id) {
	global $db;
	$sql = "select products_type from " . TABLE_PRODUCTS . " where products_id = '" . (int)$zf_product_id . "'";
	$zp_type = $db->Execute($sql);
	if ($zp_type->RecordCount() == 0) {
		return 'product_info';
	} else {
		$zp_product_type = $zp_type->fields['products_type'];
		$sql = "select type_handler from " . TABLE_PRODUCT_TYPES . " where type_id = '" . (int)$zp_product_type . "'";
		$zp_handler = $db->Execute($sql);
		return $zp_handler->fields['type_handler'] . '_info';
	}
}

function sendAboundedEmails($customers, $reminder = 1, $ps_message = '') {
	global $db, $currencies, $emails_sent;
	$customer_cart_value = 0.00;

	if ($customers->RecordCount() > 0) {
		while (!$customers->EOF) {
			// check if customer is active
			$whos_online = $db->Execute("SELECT customer_id FROM " . TABLE_WHOS_ONLINE . " WHERE customer_id = " . (int)$customers->fields['customers_id'] . " LIMIT 1;");
			// check if customer has been contacted in the last 30 days
			if((int)$reminder === 1){
				$scart = $db->Execute("SELECT customers_id FROM " . TABLE_SCART . " WHERE DATE(dateadded) >= CURDATE() - INTERVAL 30 DAY AND customers_id = " . (int)$customers->fields['customers_id'] . " LIMIT 1;");
				if(AUTOMATIC_RECOVER_CART_SALES_ENABLE_COUPON === 'true'){
					$offer_coupon = true;
				} else {
					$offer_coupon = false;
				}
			} else {
				$scart = $db->Execute("SELECT customers_id FROM " . TABLE_SECOND_SCART . " WHERE DATE(dateadded) >= CURDATE() - INTERVAL 30 DAY AND customers_id = " . (int)$customers->fields['customers_id'] . " LIMIT 1;");
				if(AUTOMATIC_RECOVER_CART_SALES_SECOND_ENABLE_COUPON === 'true'){
					$offer_coupon = true;
				} else {
					$offer_coupon = false;
				}
			}

			// send anounded emails to customers that have not been contacted within the last 30 days
			// or force sending email any way if it's manual email sent by admin ($ps_message is not empty)
			if ($whos_online->RecordCount() == 0 && $scart->RecordCount() == 0 || !empty($ps_message)) {
				// create the email
				//function to create email and coupon
				list($email, $html, $coupon_type) = create_email_and_coupon($customers, $offer_coupon, $reminder, $ps_message);

				if (($email != '') && (sizeof($html) > 0)) {

					$emails_sent++;
					$html['EMAIL_FIRST_NAME'] = $customers->fields['customers_firstname'];
                    $html['EMAIL_LAST_NAME'] = $customers->fields['customers_lastname'];

                    //$html['EMAIL_TEMPLATE_FILENAME'] = DIR_FS_EMAIL_TEMPLATES . FILE_NAME_EMAIL_TEMPLATE_AUTOMATIC_RECOVER_CART_SALES;
                    if($reminder == 2 && $offer_coupon == true){
                    	if($coupon_type == 'P'){
                    		$coupon_amount_text = AUTOMATIC_RECOVER_CART_SALES_SECOND_COUPON_AMOUNT . '%';
                    	} else {
                    		$coupon_amount_text = '$' . AUTOMATIC_RECOVER_CART_SALES_SECOND_COUPON_AMOUNT;
                    	}
                    	$email_subject = sprintf(SECOND_ABOUNDED_EMAIL_SUBJECT,$coupon_amount_text);
                    } else {
                    	$email_subject = ABOUNDED_CART_EMAIL_TEXT_SUBJECT;
                    }
					zen_mail($customers->fields['customers_firstname'], $customers->fields['customers_email_address'], $email_subject, strip_tags($email), STORE_NAME, EMAIL_FROM, $html, 'automatic_recover_cart_sales');
					echo 'Emailed '. $customers->fields['customers_email_address'] . '<br>';
					// copy admin
					//zen_mail($customers->fields['customers_firstname'], EMAIL_FROM, ABOUNDED_CART_EMAIL_TEXT_SUBJECT, $email, STORE_NAME, EMAIL_FROM, $html, 'recover_cart');
					$scart_updated = false;
					switch ($reminder) {
						case 2:
							$db->Execute("UPDATE " . TABLE_CUSTOMERS_BASKET . " SET second_reminder = 1, first_reminder = 1 WHERE customers_id = " . (int)$customers->fields['customers_id'] . ";");
							// check for scart
							$scart = $db->Execute("SELECT scartid FROM " . TABLE_SECOND_SCART . " WHERE customers_id = " . (int)$customers->fields['customers_id'] . " AND DATE(dateadded) >= CURDATE() - INTERVAL 30 DAY ORDER BY dateadded DESC LIMIT 1;");
							if ($scart->RecordCount() > 0) {
								// update
								$db->Execute("UPDATE " . TABLE_SECOND_SCART . " SET datemodified = NOW() WHERE scartid = " . $scart->fields['scartid'] . " LIMIT 1;");
							} else {
								// create new
								$db->Execute("REPLACE INTO " . TABLE_SECOND_SCART . " (customers_id, dateadded, datemodified ) VALUES (" . (int)$customers->fields['customers_id'] . ", " . date('Ymd') . ", " . date('Ymd') . ");");
							}
						break;
						// do not break as we also need to mark the first_reminder
						case 1:
						default:
							$db->Execute("UPDATE " . TABLE_CUSTOMERS_BASKET . " SET first_reminder = 1 WHERE customers_id = " . (int)$customers->fields['customers_id'] . ";");
							if (!$scart_updated) {
								// check for scart
								$scart = $db->Execute("SELECT scartid FROM " . TABLE_SCART . " WHERE customers_id = " . (int)$customers->fields['customers_id'] . " AND DATE(dateadded) >= CURDATE() - INTERVAL 30 DAY ORDER BY dateadded DESC LIMIT 1;");
								if ($scart->RecordCount() > 0) {
									// update
									$db->Execute("UPDATE " . TABLE_SCART . " SET datemodified = " . date('Ymd') . " WHERE scartid = " . $scart->fields['scartid'] . " LIMIT 1;");
								} else {
									// create new
									$db->Execute("REPLACE INTO " . TABLE_SCART . " (customers_id, dateadded, datemodified ) VALUES (" . (int)$customers->fields['customers_id'] . ", " . date('Ymd') . ", " . date('Ymd') . ");");
								}
							}
						break;
					}
					// update to avoid duplicate emails
				}
			}
			$customers->MoveNext();
		}
		echo '<p>' . (int)$emails_sent . ' emails sent.</p>';
	} else {
		if (basename($_SERVER['PHP_SELF']) != 'recover_cart_sales.php') {
			$subscribed_notification = '';
			if (AUTOMATIC_RECOVER_CART_SUBSCRIBED_ONLY == 'true') {
				$subscribed_notification = ' for subscribed customers';
			}

			echo '<p>No baskets found'. $subscribed_notification .'.</p>';
		}

	}

}

function create_email_and_coupon($customers, $offer_coupon = true, $reminder_email = 1, $ps_message = ''){
	global $db, $currencies;
	$email = '';
	$html = array();
	$products_ordered = array();
	$also_purchased = array();
	$customers_basket = $db->Execute("SELECT cb.customers_basket_id, cb.customers_id, cb.products_id, cb.customers_basket_quantity FROM " . TABLE_CUSTOMERS_BASKET . " cb
			WHERE cb.customers_id = " . (int)$customers->fields['customers_id'] . ";"
	);

	while (!$customers_basket->EOF) {
		$product = $db->Execute("SELECT p.products_id, pd.products_name FROM " . TABLE_PRODUCTS . " p
				LEFT JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON (pd.products_id = p.products_id)
				WHERE p.products_id = " . (int)$customers_basket->fields['products_id'] . "
				AND p.products_status = 1
				LIMIT 1;"
		);
		if ($product->RecordCount() > 0) {
			$products_actual_price = zen_get_products_actual_price((int)$product->fields['products_id']);
			$products_ordered[(int)$product->fields['products_id']] = array(
					'products_name' => $product->fields['products_name'],
					'products_quantity' => $customers_basket->fields['customers_basket_quantity'],
					'regular_price' => $currencies->format($products_actual_price),
					'products_image' => str_replace('src="', 'style="display: block;" src="' . HTTP_SERVER, zen_get_products_image((int)$product->fields['products_id'], 134, 134)),
					'products_url' => (IS_ADMIN_FLAG !== true ? zen_href_link(zen_get_info_page((int)$product->fields['products_id']), 'products_id=' . (int)$product->fields['products_id'], 'NONSSL') : zen_href_link(zen_get_info_page_rcs((int)$product->fields['products_id']), 'products_id=' . (int)$product->fields['products_id'], 'NONSSL'))
			);
			$customer_cart_value .= (double)$products_actual_price;

		} else {
			// delete the customer's basket as this product no longer exists
			$db->Execute("DELETE FROM " . TABLE_CUSTOMERS_BASKET . " WHERE customers_id = " . (int)$customers->fields['customers_id'] . " AND products_id = " . (int)$customers_basket->fields['products_id'] . ";");
		}
		$customers_basket->MoveNext();
	}

	$product_line_items = '';
	$last_item = count($products_ordered) - 1;
	$x = 0;
	foreach($products_ordered as $products_id => $product_details) {
		$x++;

		// Omit extra space for last item
		if ($x > $last_item) {
			$product_line_items .= $product_details['products_quantity'] . ' x <a href="' . $product_details['products_url'] . '">' . $product_details['products_name'] . "</a>\n";
		}

		else {
			$product_line_items .= $product_details['products_quantity'] . ' x <a href="' . $product_details['products_url'] . '">' . $product_details['products_name'] . "</a>\n\n";
		}
	}

    if ($offer_coupon) {
		// Create Coupon
		$code = zen_create_coupon_code_rcs();

		if($reminder_email == 1){
			// Get discount amount and type, if it has percent in value
			if (strpos(AUTOMATIC_RECOVER_CART_SALES_COUPON_AMOUNT, '%') !== false) {
				$coupon_type = 'P';
				$amount = (double)str_replace("%", "", AUTOMATIC_RECOVER_CART_SALES_COUPON_AMOUNT);
			} else {
				$amount = (double)AUTOMATIC_RECOVER_CART_SALES_COUPON_AMOUNT;
				$coupon_type = 'F';
			}
			$expiry = '+' . (int)AUTOMATIC_RECOVER_CART_SALES_COUPON_EXPIRY . ' day';
			$expiry = date('Y-m-d H:i:s', strtotime($expiry));
			$min_amount = AUTOMATIC_RECOVER_CART_SALES_COUPON_MIN_AMOUNT;
			if (!defined('AUTOMATIC_RECOVER_CART_SALES_RESTRICT_CATEGORY')) {
				$restricted_categories = array(
					array(
						'category_id' =>  (int)AUTOMATIC_RECOVER_CART_SALES_RESTRICT_CATEGORY,
						'coupon_restrict' => 'N',
						),
					);
			}
			if (!defined('AUTOMATIC_RECOVER_CART_SALES_RESTRICT_PRODUCT')) {
				$restricted_products = array(
					array(
						'product_id' =>  (int)AUTOMATIC_RECOVER_CART_SALES_RESTRICT_PRODUCT,
						'coupon_restrict' => 'N',
						),
					);
			}
		} else {
			// Get discount amount and type, if it has percent in value
			if (strpos(AUTOMATIC_RECOVER_CART_SALES_SECOND_COUPON_AMOUNT, '%') !== false) {
				$coupon_type = 'P';
				$amount = (double)str_replace("%", "", AUTOMATIC_RECOVER_CART_SALES_SECOND_COUPON_AMOUNT);
			} else {
				$amount = (double)AUTOMATIC_RECOVER_CART_SALES_SECOND_COUPON_AMOUNT;
				$coupon_type = 'F';
			}
			$expiry = '+' . (int)AUTOMATIC_RECOVER_CART_SALES_SECOND_COUPON_EXPIRY . ' day';
			$expiry = date('Y-m-d H:i:s', strtotime($expiry));
			$min_amount = AUTOMATIC_RECOVER_CART_SALES_SECOND_COUPON_MIN_AMOUNT;
			if (!defined('AUTOMATIC_RECOVER_CART_SALES_SECOND_RESTRICT_CATEGORY')) {
				$restricted_categories = array(
					array(
						'category_id' =>  (int)AUTOMATIC_RECOVER_CART_SALES_SECOND_RESTRICT_CATEGORY,
						'coupon_restrict' => 'N',
						),
					);
			}
			if (!defined('AUTOMATIC_RECOVER_CART_SALES_SECOND_RESTRICT_PRODUCT')) {
				$restricted_products = array(
					array(
						'product_id' =>  (int)AUTOMATIC_RECOVER_CART_SALES_SECOND_RESTRICT_PRODUCT,
						'coupon_restrict' => 'N',
						),
					);
			}
		}

		$uses_per_coupon = 1;
		$uses_per_user = 1;
		$sales_eligible = 0;
		$specials_eligible = 0;
		$coupon_zone_restriction = 0;
		$manufacturer_ids = 0;

		// Override admin settings with template_recovercart coupon
		if (AUTOMATIC_RECOVER_CART_SALES_COUPON_OVERRIDE == 'true') {
			$template_values = $db->Execute("SELECT * FROM " . TABLE_COUPONS . "
					WHERE coupon_code = 'template_recovercart';"
			);
			if ($template_values->RecordCount() > 0) {

				$amount = (double)$template_values->fields['coupon_amount'];
				$min_amount = (double)$template_values->fields['coupon_minimum_order'];
				$coupon_type = $template_values->fields['coupon_type'];
				//$expiry = $template_values->fields['coupon_expire_date'];
				$uses_per_coupon = $template_values->fields['uses_per_coupon'];
				$uses_per_user = $template_values->fields['uses_per_user'];
				$coupon_zone_restriction = $template_values->fields['coupon_zone_restriction'];
				// $manufacturer_ids = $template_values->fields['manufacturer_ids'];
				// $sales_eligible = $template_values->fields['sales_eligible'];
				// $specials_eligible = $template_values->fields['specials_eligible'];
			}
			$restricted_categories = array();
			$restricted_products = array();
			$template_values = $db->Execute("SELECT cr.product_id, cr.category_id, cr.coupon_restrict
				FROM " . TABLE_COUPON_RESTRICT . " cr
				LEFT JOIN " . TABLE_COUPONS . " c ON (c.coupon_id = cr.coupon_id)
				WHERE c.coupon_code = 'template_recovercart'
				AND (cr.product_id != 0 OR cr.category_id != 0)");
			if ($template_values->RecordCount() > 0) {
				while (!$template_values->EOF) {
					if ($template_values->fields['category_id'] != 0) {
						$restricted_categories[] = array(
							'category_id' =>  $template_values->fields['category_id'],
							'coupon_restrict' => $template_values->fields['coupon_restrict'],
							);
					}
					if ($template_values->fields['product_id'] != 0) {
						$restricted_products[] = array(
							'product_id' =>  $template_values->fields['product_id'],
							'coupon_restrict' => $template_values->fields['coupon_restrict'],
							);
					}
					$template_values->MoveNext();
				}
			}
		}
		// EOF Override admin settings with template_recovercart coupon

		$sql_data_array = array(
				'coupon_code' => $code,
				'coupon_amount' => $amount,
				'coupon_minimum_order' => (double)$min_amount,
				'coupon_type' => $coupon_type,
				'coupon_start_date' => 'now()',
				'coupon_expire_date' => $expiry,
				'uses_per_coupon' => $uses_per_coupon,
				'uses_per_user' => $uses_per_user,
				'coupon_zone_restriction' => $coupon_zone_restriction,
				// 'manufacturer_ids' => $manufacturer_ids,
				// 'sales_eligible' => $sales_eligible,
				// 'specials_eligible' => $specials_eligible,
				'date_created' => 'now()',
				'date_modified' => 'now()'
		);
		zen_db_perform(TABLE_COUPONS, $sql_data_array);

		$insert_id = $db->Insert_ID();

		// insert data into coupons restrict
		if (!empty($restricted_categories)) {
			foreach ($restricted_categories as $restricted_category) {
				$sql_data_array = array(
				'coupon_id' => $insert_id,
				'product_id' => 0,
				);
				$sql_data_array = array_merge($sql_data_array, $restricted_category);
				zen_db_perform(TABLE_COUPON_RESTRICT, $sql_data_array);
			}
		}

		if (!empty($restricted_products)) {
			foreach ($restricted_products as $restricted_product) {
				$sql_data_array = array(
				'coupon_id' => $insert_id,
				'category_id' => 0,
				);
				$sql_data_array = array_merge($sql_data_array, $restricted_product);
				zen_db_perform(TABLE_COUPON_RESTRICT, $sql_data_array);
			}
		}

		// insert data into coupons description
		$sql_data_array = array(
				'coupon_id' => $insert_id,
				'language_id' => 1,
				'coupon_name' => 'Recover Cart'
		);
		zen_db_perform(TABLE_COUPONS_DESCRIPTION, $sql_data_array);

        $email_text = ABOUNDED_CART_EMAIL_TEXT_COUPON_INTRO . sprintf(ABOUNDED_CART_EMAIL_TEXT_COUPON, $code);
    }

    $email_text = '';
	$email = '';
	$email .= ABOUNDED_CART_EMAIL_TEXT_CURCUST_INTRO . ABOUNDED_CART_EMAIL_TEXT_BODY_HEADER . $product_line_items ;

    if( EMAIL_USE_HTML == 'true' && $customers->fields['customers_email_format'] == 'HTML' ){
        $email .= '<div style="padding-top: 20px; padding-bottom: 20px; text-align: center;"><a href="' . zen_href_link(FILENAME_LOGIN) . '" style="border-top: 10px solid #ff6500; border-bottom: 10px solid #ff6500; border-left: 10px solid #ff6500; border-right: 10px solid #ff6500; background-color: #ff6500; display: inline-block; color: white; font-size: 16px;">' . RECOVER_SHOP_NOW . '</a></div>';
    }    else {
        $email .= "\n" . zen_href_link(FILENAME_LOGIN);
    }

	// Check if percent of or dollar amount off
	if ($coupon_type == 'P') {
		$amount = $amount . '%';
	}	else {
		$amount = $currencies->format((float)$amount);
	}

	// coupon restricts
	$restricted_string = '';
	if (!empty($restricted_categories)) {
		$restricted_categories_ids = array();
		foreach ($restricted_categories as $restricted_category) {
			$restricted_categories_ids[] = $restricted_category['category_id'];
		}
		$restricted_category_select = $db->Execute('SELECT categories_name FROM ' . TABLE_CATEGORIES_DESCRIPTION . '
		WHERE categories_id IN (' . implode(',', $restricted_categories_ids) . ')');
		$restricted_categories_names = array();
		while (!$restricted_category_select->EOF) {
			$restricted_categories_names[] = $restricted_category_select->fields['categories_name'];
			$restricted_category_select->MoveNext();
		}
		if (!empty($restricted_categories_names)) {
			$restricted_string = 'with ' . implode(',', $restricted_categories_names) . ' items';
		}
	}

	if (!empty($restricted_products)) {
		$restricted_products_ids = array();
		foreach ($restricted_products as $restricted_product) {
			$restricted_products_ids[] = $restricted_product['product_id'];
		}
		$restricted_product_select = $db->Execute('SELECT products_name FROM ' . TABLE_PRODUCTS_DESCRIPTION . '
		WHERE products_id IN (' . implode(',', $restricted_products_ids) . ')');
		$restricted_products_names = array();
		while (!$restricted_product_select->EOF) {
			$restricted_products_names[] = $restricted_product_select->fields['products_name'];
			$restricted_product_select->MoveNext();
		}
		if (!empty($restricted_products_names)) {
			$restricted_string = 'with ' . implode(',', $restricted_categories_names) . ' items or ' . implode(',', $restricted_products_names);
		}
	}

	// min order
	$min_amount_string = '';
	if (($min_amount != '') &&
			($min_amount != 0) &&
			($min_amount != '0') &&
			($min_amount != '0.00')) {
		$min_amount_string = ' with a minimum order of ' . $min_amount;
	}

	if($offer_coupon){
	    if ((int)AUTOMATIC_RECOVER_CART_SALES_COUPON_AMOUNT > 0 || AUTOMATIC_RECOVER_CART_SALES_SECOND_COUPON_AMOUNT > 0 || AUTOMATIC_RECOVER_CART_SALES_COUPON_OVERRIDE == 'true') {
		    $email_text = sprintf(ABOUNDED_CART_EMAIL_TEXT_COUPON_INTRO, $amount, $restricted_string, $expiry, $min_amount_string) . sprintf(ABOUNDED_CART_EMAIL_TEXT_COUPON, $code);
	    }
	} else {
		$email_text = '';
	}

	if( EMAIL_USE_HTML == 'true' && $customers->fields['customers_email_format'] == 'HTML' ) {
		$email .= '<div id="coupon-section" style="font-weight: bold;">'. $email_text . '</div>' . ABOUNDED_CART_EMAIL_TEXT_BODY_FOOTER;
	}    else {
		$email .= "\n" . $email_text . ABOUNDED_CART_EMAIL_TEXT_BODY_FOOTER;
	}

	if( EMAIL_USE_HTML == 'true' && $customers->fields['customers_email_format'] == 'HTML' ){

		$email .= 'Regards, '. "\n" . STORE_OWNER . "\n" . ' <a href="' . zen_href_link(FILENAME_DEFAULT) . '">' . STORE_NAME . "<a>\n";
	}    else {
		$email .= 'Regards, '. "\n" . STORE_OWNER . "\n" . zen_href_link(FILENAME_DEFAULT);
	}

	// PS Message
	if ($ps_message != '') {
		$email .= "\n\n" . 'PS. ' . $ps_message;
	}

	$email .= "\n\n";

	$email .= "\n" . EMAIL_SEPARATOR . "\n\n";
	$email .= ABOUNDED_CART_EMAIL_TEXT_LOGIN;

	if( EMAIL_USE_HTML == 'true' && $customers->fields['customers_email_format'] == 'HTML' ){
		$email .= '  <a href="' . zen_href_link(FILENAME_LOGIN, '', 'SSL') . '">here</a>';
	}    else {
		$email .= '  (' . zen_href_link(FILENAME_LOGIN, '', 'SSL') . ')';
	}


	$html['EMAIL_MESSAGE_HTML'] = nl2br($email);

	return array(
		$email,$html, $coupon_type
	);
}

// Execute if cron is enabled from configuration
if (AUTOMATIC_RECOVER_CART_SALES_CRON == 'true') {

	// Subscribe check
	$subscribe_check = '';
	if (AUTOMATIC_RECOVER_CART_SUBSCRIBED_ONLY == 'true') {
		$subscribe_check = ' AND c.customers_newsletter = 1 ';
	}

	// send reminder
	$customers = $db->Execute("SELECT DISTINCT(cb.customers_id), c.customers_firstname, c.customers_lastname, c.customers_email_address, c.customers_email_format FROM " . TABLE_CUSTOMERS_BASKET . " cb
		LEFT JOIN " . TABLE_CUSTOMERS . " c ON (c.customers_id = cb.customers_id)
		WHERE cb.customers_basket_date_added <= (NOW() - INTERVAL 30 MINUTE)
		AND cb.customers_basket_date_added >= (NOW() - INTERVAL 1 DAY)
		AND cb.first_reminder = 0 " . $subscribe_check .
			" GROUP BY cb.customers_id
		ORDER BY cb.customers_id ASC;"
	);

	sendAboundedEmails($customers, 1);

	$coupon_customers = $db->Execute("SELECT DISTINCT(cb.customers_id), c.customers_firstname, c.customers_lastname, c.customers_email_address, c.customers_email_format FROM " . TABLE_CUSTOMERS_BASKET . " cb
		LEFT JOIN " . TABLE_CUSTOMERS . " c ON (c.customers_id = cb.customers_id)
		WHERE cb.customers_basket_date_added <= (NOW() - INTERVAL 3 DAY)
		AND cb.first_reminder = 1 AND cb.second_reminder = 0 " . $subscribe_check .
			" GROUP BY cb.customers_id ORDER BY cb.customers_id ASC;"
	);

	sendAboundedEmails($coupon_customers, 2);
}

else {
	echo "The Enable Cron option must be enabled in configuration.";
}



<?php
/**
 * automatic_recover_cart_sales.php
 *
 * @package Automatic Recover Cart Sales
 * @copyright Copyright 2003-2016 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: automatic_recover_cart_sales.php 2 2016-02-05 01:19:41Z numinix $
 */

define('BOX_CONFIGURATION_AUTOMATIC_RECOVER_CART_SALES', 'Automatic Recover Cart Sales Configuration');


<?php

$define = [
    'BOX_REPORTS_RECOVER_CART_SALES' => 'Recovered Sales Results',
    'BOX_TOOLS_RECOVER_CART' => 'Recover Cart Sales',
    'BOX_CONFIG_RECOVER_CART_SALES' => 'Configure RCS'
];

$zc158 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= '5.8'));
if ($zc158) {
    return $define;
} else {
    nmx_create_defines($define);
}

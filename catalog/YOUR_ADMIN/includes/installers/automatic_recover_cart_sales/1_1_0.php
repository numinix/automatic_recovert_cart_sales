<?php
// Added send to subscribed only option

$db->Execute("INSERT IGNORE INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES
('Only email subscribed customers', 'AUTOMATIC_RECOVER_CART_SUBSCRIBED_ONLY', 'true', 'Send emails only to customers who are subscribed.', " . $configuration_group_id . ", 49, NOW(), NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),');");

<?php

// Add first_reminder column
if (!$sniffer->field_exists(TABLE_CUSTOMERS_BASKET, 'second_reminder')) $db->Execute("ALTER TABLE " . TABLE_CUSTOMERS_BASKET . " ADD second_reminder TINYINT( 1 ) NOT NULL DEFAULT 0;");

if(!defined('AUTOMATIC_RECOVER_CART_SALES_SECOND_ENABLE_COUPON')) $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Enable second email coupon', 'AUTOMATIC_RECOVER_CART_SALES_SECOND_ENABLE_COUPON', 'false', 'Enable coupon for second email?', " . $configuration_group_id . ", 53, NOW(), NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),')");
if(!defined('AUTOMATIC_RECOVER_CART_SALES_SECOND_COUPON_MIN_AMOUNT')) $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Second email coupon minimum amount ($)', 'AUTOMATIC_RECOVER_CART_SALES_SECOND_COUPON_MIN_AMOUNT', '0', 'Set the minimum order value before the coupon is valid.', " . $configuration_group_id . ", 54, NOW(), NOW(), NULL, NULL)");
if(!defined('AUTOMATIC_RECOVER_CART_SALES_ENABLE_COUPON')) $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Enable coupon', 'AUTOMATIC_RECOVER_CART_SALES_ENABLE_COUPON', 'false', 'Enable coupon for first email?', " . $configuration_group_id . ", 55, NOW(), NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),')");
if(!defined('AUTOMATIC_RECOVER_CART_SALES_SECOND_RESTRICT_PRODUCT')) $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Second email coupon restrict to product', 'AUTOMATIC_RECOVER_CART_SALES_SECOND_RESTRICT_PRODUCT', '0', 'Enter a product_id you would like to restrict the coupon to a category.', " . $configuration_group_id . ", 56, NOW(), NOW(), NULL, NULL)");
if(!defined('AUTOMATIC_RECOVER_CART_SALES_SECOND_RESTRICT_CATEGORY')) $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Second email coupon restrict to category', 'AUTOMATIC_RECOVER_CART_SALES_SECOND_RESTRICT_CATEGORY', '0', 'Enter a category_id you would like to restrict the coupon to a category.', " . $configuration_group_id . ", 57, NOW(), NOW(), NULL, NULL)");
if(!defined('AUTOMATIC_RECOVER_CART_SALES_SECOND_COUPON_EXPIRY')) $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Second email coupon expiry (Days)', 'AUTOMATIC_RECOVER_CART_SALES_SECOND_COUPON_EXPIRY', '0', 'Enter the number of days the coupon for will be valid for.', " . $configuration_group_id . ", 58, NOW(), NOW(), NULL, NULL)");
if(!defined('AUTOMATIC_RECOVER_CART_SALES_SECOND_COUPON_AMOUNT')) $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Second email coupon amount', 'AUTOMATIC_RECOVER_CART_SALES_SECOND_COUPON_AMOUNT', '5', 'The value of the discount for the coupon, either fixed or add a % on the end for a percentage discount.', " . $configuration_group_id . ", 59, NOW(), NOW(), NULL, NULL);");

$db->Execute(
"CREATE TABLE IF NOT EXISTS " . TABLE_SECOND_SCART . " (
  scartid int(11) NOT NULL auto_increment,
  customers_id int(11) NOT NULL,
  dateadded date NOT NULL,
  datemodified date NOT NULL,
  PRIMARY KEY  (scartid),
  UNIQUE KEY customers_id (customers_id),
  UNIQUE KEY scartid (scartid)
) ENGINE=MyISAM;"
);
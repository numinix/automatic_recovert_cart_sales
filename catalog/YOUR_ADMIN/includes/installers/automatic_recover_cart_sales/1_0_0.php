<?php

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES
('Enable Cron', 'AUTOMATIC_RECOVER_CART_SALES_CRON', 'true', 'Enable or disable the daily Cron from running.', " . $configuration_group_id . ", 42, NOW(), NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
('Coupon amount', 'AUTOMATIC_RECOVER_CART_SALES_COUPON_AMOUNT', 10, 'The value of the discount for the coupon, either fixed or add a % on the end for a percentage discount.', " . $configuration_group_id . ", 45, NOW(), NOW(), NULL, NULL),
('Coupon expiry (Days)', 'AUTOMATIC_RECOVER_CART_SALES_COUPON_EXPIRY', 2, 'Enter the number of days the coupon will be valid for.', " . $configuration_group_id . ", 45, NOW(), NOW(), NULL, NULL),
('Coupon restrict to category', 'AUTOMATIC_RECOVER_CART_SALES_RESTRICT_CATEGORY', 0, 'Enter a category_id you would like to restrict the coupon to a category.', " . $configuration_group_id . ", 45, NOW(), NOW(), NULL, NULL),
('Coupon restrict to product', 'AUTOMATIC_RECOVER_CART_SALES_RESTRICT_PRODUCT', 0, 'Enter a product_id you would like to restrict the coupon to a category.', " . $configuration_group_id . ", 45, NOW(), NOW(), NULL, NULL);" );


$zc150 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= 5));
if ($zc150) { // continue Zen Cart 1.5.0
	// add configuration menu
	if (!zen_page_key_exists('configAUTOMATIC_RECOVER_CART_SALES')) {
		zen_register_admin_page('configAUTOMATIC_RECOVER_CART_SALES', 'BOX_CONFIGURATION_AUTOMATIC_RECOVER_CART_SALES', 'FILENAME_CONFIGURATION', 'gID=' . $configuration_group_id, 'configuration', 'Y', $configuration_group_id);

		$messageStack->add('Installed Automatic Recover Cart Sales Configuration menu.', 'success');
	}
}

// Add first_reminder column
if (!$sniffer->field_exists(TABLE_CUSTOMERS_BASKET, 'first_reminder')) {
	$db->Execute("ALTER TABLE " . TABLE_CUSTOMERS_BASKET . " ADD first_reminder TINYINT( 1 ) NOT NULL DEFAULT 0;");
}


<?php

// Added template_recovercart coupon override

// Add override configuration setting
$db->Execute("INSERT IGNORE INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES
('Override coupon settings', 'AUTOMATIC_RECOVER_CART_SALES_COUPON_OVERRIDE', 'false', 'Use the settings of the template_recovercart coupon rather than the settings on this page.', " . $configuration_group_id . ", 52, NOW(), NOW(), NULL,  'zen_cfg_select_option(array(\'true\', \'false\'),');");

// Add coupon template
// $db->Execute("INSERT IGNORE INTO " . TABLE_COUPONS . " (
//     coupon_id ,
//     coupon_type ,
//     coupon_code ,
//     coupon_amount ,
//     coupon_minimum_order ,
//     coupon_start_date ,
//     coupon_expire_date ,
//     uses_per_coupon ,
//     uses_per_user ,
//     restrict_to_products ,
//     restrict_to_categories ,
//     restrict_to_customers ,
//     coupon_active ,
//     date_created ,
//     date_modified ,
//     coupon_zone_restriction ,
//     manufacturer_ids ,
//     sales_eligible ,
//     specials_eligible
//     )
//     VALUES (NULL , 'P', 'coupkb', '5.0000', '0.0000', '2000-03-07 00:00:00', '2050-03-07 00:00:00', '1', '1', '', '', NULL ,
//      'Y', '2016-03-07 09:01:20', '2016-03-07 09:01:20', '0', '0', '1', '1');");


$db->Execute("INSERT IGNORE INTO " . TABLE_COUPONS . " (
    coupon_id ,
    coupon_type ,
    coupon_code ,
    coupon_amount ,
    coupon_minimum_order ,
    coupon_start_date ,
    coupon_expire_date ,
    uses_per_coupon ,
    uses_per_user ,
    restrict_to_products ,
    restrict_to_categories ,
    restrict_to_customers ,
    coupon_active ,
    date_created ,
    date_modified ,
    coupon_zone_restriction ,
    coupon_calc_base,
    coupon_order_limit,
    coupon_is_valid_for_sales,
    coupon_product_count	
    )
    VALUES (NULL , 'P', 'coupkb', '5.0000', '0.0000', '2000-03-07 00:00:00', '2050-03-07 00:00:00', '1', '1', '', '', NULL ,
     'Y', '2016-03-07 09:01:20', '2016-03-07 09:01:20', '0', '0', '0', '1','1');");
     
// Add coupon template description
$get_id = $db->Insert_ID();
$db->Execute("INSERT IGNORE INTO " . TABLE_COUPONS_DESCRIPTION . " (
    coupon_id ,
    language_id ,
    coupon_name ,
    coupon_description
    )
    VALUES (" . (int)$get_id . ", '" . (int)$_SESSION['languages_id'] . "', 'template_recovercart', '');");
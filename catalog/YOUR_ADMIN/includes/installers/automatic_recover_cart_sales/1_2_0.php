<?php

// Added F discount option to RCS and added Minimum amount option

$db->Execute("INSERT IGNORE INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES
('Coupon minimum amount ($)', 'AUTOMATIC_RECOVER_CART_SALES_COUPON_MIN_AMOUNT', '0', 'Set the minimum order value before the coupon is valid.', " . $configuration_group_id . ", 48, NOW(), NOW(), NULL, NULL);");

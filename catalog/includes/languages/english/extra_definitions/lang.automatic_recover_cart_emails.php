<?php
// define('HEADING_IN_YOUR_CART', 'In Your Cart:');
// define('ABOUNDED_CART_EMAIL_TEXT_LOGIN', 'Login to your account');
// define('EMAIL_SEPARATOR', '------------------------------------------------------');
// define('ABOUNDED_CART_EMAIL_TEXT_SUBJECT', 'Special Offer From '.  STORE_NAME );
// define('ABOUNDED_CART_EMAIL_TEXT_SALUTATION', 'Hi ' );
// define('ABOUNDED_CART_EMAIL_TEXT_CURCUST_INTRO', "\n\n" . 'Thank you for stopping by ' . STORE_NAME .
//     ' and considering us for your purchase. ');

// define('ABOUNDED_CART_EMAIL_TEXT_BODY_HEADER',
// 'We noticed that during a visit to our store you placed ' .
// 'the following item(s) in your shopping cart, but did not complete ' .
// 'the transaction.' . "\n\n" .
// 'In your cart, you left:' . "\n\n"
// );

// define('ABOUNDED_CART_EMAIL_TEXT_COUPON_INTRO',
//     'But it\'s not too late! To complete your purchase, visit our store and use the following coupon code on the checkout page for an additional %s ' .
//     'off your next order %s if you order before %s%s:' . "\n\n");

// define('ABOUNDED_CART_EMAIL_TEXT_COUPON', 'Coupon code <strong>%s</strong>' . "\n\n");

// define('ABOUNDED_CART_EMAIL_TEXT_BODY_FOOTER',
//     'We really do take pride in the products we sell and the way we treat our customers ' .
//     'so feel free to contact us if you have any questions by simply replying to this email. '."\n\n".
//     'Thanks for shopping!' .
//     "\n\n" . STORE_NAME . "\n\n"
// );

// define('RECOVER_SHOP_NOW', 'Shop Now');
// define('SECOND_ABOUNDED_EMAIL_SUBJECT', '%s Coupon now available at ' . STORE_NAME);

$define = [
    'HEADING_IN_YOUR_CART' => 'In Your Cart:',
    'ABOUNDED_CART_EMAIL_TEXT_LOGIN' => 'Login to your account',
    'EMAIL_SEPARATOR' => '------------------------------------------------------',
    'ABOUNDED_CART_EMAIL_TEXT_SUBJECT' => 'Special Offer From '.  STORE_NAME ,
    'ABOUNDED_CART_EMAIL_TEXT_SALUTATION' => 'Hi ' ,
    'ABOUNDED_CART_EMAIL_TEXT_CURCUST_INTRO' => "\n\n" . 'Thank you for stopping by ' . STORE_NAME .
        ' and considering us for your purchase. ',
    
    'ABOUNDED_CART_EMAIL_TEXT_BODY_HEADER' =>
    'We noticed that during a visit to our store you placed ' .
    'the following item(s) in your shopping cart, but did not complete ' .
    'the transaction.' . "\n\n" .
    'In your cart, you left:' . "\n\n"
    ,
    
    'ABOUNDED_CART_EMAIL_TEXT_COUPON_INTRO' =>
        'But it\'s not too late! To complete your purchase, visit our store and use the following coupon code on the checkout page for an additional %s ' .
        'off your next order %s if you order before %s%s:' . "\n\n",
    
    'ABOUNDED_CART_EMAIL_TEXT_COUPON' => 'Coupon code <strong>%s</strong>' . "\n\n",
    
    'ABOUNDED_CART_EMAIL_TEXT_BODY_FOOTER' =>
        'We really do take pride in the products we sell and the way we treat our customers ' .
        'so feel free to contact us if you have any questions by simply replying to this email. '."\n\n".
        'Thanks for shopping!' .
        "\n\n" . STORE_NAME . "\n\n"
    ,
    
    'RECOVER_SHOP_NOW' => 'Shop Now',
    'SECOND_ABOUNDED_EMAIL_SUBJECT' => '%s Coupon now available at ' . STORE_NAME
];

$zc158 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= '5.8'));
if ($zc158) {
    return $define;
} else {
    nmx_create_defines($define);
}